#include<stdio.h>
#include<fcntl.h>
#include<errno.h>

int main()
{
	int fd,ret;
	fd = open("/dev/test1",O_RDWR);

	if(fd<0)
	{
		perror("error opening the device\n");
		return -1;
	}

	ret = read(fd,NULL,0);

	if(ret>0)
	{
		printf("data read is nothing \n");
	}
	else
	{
		perror("error in reading \n");
	}
	return 0;
}
