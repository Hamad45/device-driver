obj-m=test.o
KERN_DIR=/lib/modules/$(shell uname -r)/build
PWD=$(shell pwd)
modules:
	$(MAKE) -C ${KERN_DIR} M=${PWD} modules
CC:
	gcc -o read read.c
	gcc -o read1 read1.c
	gcc -o write write.c
clean:
	$(MAKE) -C ${KERN_DIR} M=${PWD} clean
