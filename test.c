#include<linux/module.h>
#include<linux/init.h>
#include<linux/cdev.h>
#include<linux/fs.h>
#include<linux/circ_buf.h>
#include<linux/slab.h>
#include<linux/sched.h>
#include<asm/uaccess.h>
#include<linux/kdev_t.h>

#define MAJORNO 42
#define  N_MINORS 3
#define SIZE 32


MODULE_LICENSE("GPL");
MODULE_AUTHOR("CDAC");

static unsigned int temp;
dev_t deviceno;

static struct tdev
{
         struct circ_buf cbuf;
         struct cdev _cdev;
}_tdev[N_MINORS];

int  module_open(struct inode *inodp, struct file *filp)
{
        printk(KERN_ALERT "Inside the %s Function \n",__FUNCTION__);
        filp->private_data=container_of(inodp->i_cdev,struct tdev,_cdev);
	temp = MINOR(inodp->i_rdev);
        return 0;
}

int module_release(struct inode *inodp, struct file *filp)
{
        printk(KERN_ALERT "Inside the %s Function \n", __FUNCTION__);
        return 0;
}

ssize_t module_read (struct file *filp, char __user *ubuff, size_t cnt, loff_t *off)
{
        int ret,i,m;
        printk(KERN_ALERT "Insdie the %s Function \n",__FUNCTION__);
        struct tdev *_tdev=filp->private_data;

        m=min((int)cnt,(int)CIRC_CNT(_tdev->cbuf.head,_tdev->cbuf.tail,SIZE));

        for(i=0;i<m;i++)
        {
                ret=copy_to_user(ubuff+i,_tdev->cbuf.buf+_tdev->cbuf.tail,1);
                if(ret)
                {
                         printk("\nError in Copying to user");
                         return -1;
                }
                _tdev->cbuf.tail=(_tdev->cbuf.tail+1)&(SIZE-1);
        }

        printk("Read %d bytes\n",m);

        	switch(temp)
		{
		case 0 :
                                printk("Hii i am from Device 1");
                                break;

		case 1 :
                                printk("Hii i am from Device 2");
                                break;

                 case 2 :
                                printk("Hii i am from Device 3");
                                break;

               	 default :
                                printk("Invalid operation");

                  }
        return m;
}
ssize_t module_write (struct file *filp,const char __user *ubuff, size_t cnt, loff_t *off)
{

        int ret,i,m;
        printk(KERN_ALERT "Inside the %s Function \n",__FUNCTION__);

        struct tdev *_tdev=filp->private_data;
        m=min((int)cnt,(int)CIRC_SPACE(_tdev->cbuf.head,_tdev->cbuf.tail,SIZE));

        for(i=0;i<m;i++)
        {
                ret=copy_from_user(_tdev->cbuf.buf+_tdev->cbuf.head,ubuff+i,1);

                if(ret)
                {
                         printk("Error while copying to Kernel\n");
                         return -1;
                }
                _tdev->cbuf.head = (_tdev->cbuf.head+1)&(SIZE-1);
        }
        printk("Wrote %d\n", i);

        switch(temp)
        {
                case 0 :
                                printk("A write in Device 1");
                                break;

                case 1 :
                                printk("A write in Device 2");
                                break;

                case 2 :
                                printk("A write in Device 3");
                               break;

                default :
                                printk("Invalid operation");
	}

        return m;
}
static struct file_operations fops=
{
                .owner=THIS_MODULE,
                .open=module_open,
                .release=module_release,
                .read=module_read,
                .write=module_write,
};

static __init int driv_init(void)
{
        int ret,i,j;
        printk("\n Inside driv_init\n");
        deviceno = MKDEV(42,0);
        ret=register_chrdev_region(deviceno,N_MINORS,"testdevice");

        if(ret)
        {
                printk("Error:\n");
                return -1;
        }

        for(i=0;i<N_MINORS;i++)
        {
               _tdev[i].cbuf.buf=kmalloc(SIZE,GFP_KERNEL);
                if(!_tdev[i].cbuf.buf)
		{

                        goto cbuf_err;
        	}
	}
       for(j=0;j<N_MINORS;j++)
        {
                 dev_t tempdev;
                tempdev = MKDEV(MAJOR(deviceno),MINOR(deviceno)+j);

                cdev_init(&_tdev[j]._cdev,&fops);
                ret=cdev_add(&_tdev[j]._cdev,tempdev,1);

		 if(ret)
                {
                        goto cdev_err;
                }
        }
        return 0;

cbuf_err:
                for(--i;i<=0;i--)
                {
                        kfree(_tdev[i].cbuf.buf);
                }

cdev_err:
                for(--j;j<=0;j--)
                {
                      cdev_del(&_tdev[j]._cdev);
                unregister_chrdev_region(deviceno,N_MINORS);
		}
                return -1;
}

static __exit void driv_exit(void)
{
        int i,j;
        printk("\n GOODBYE driv\n");

        for(i=0;i<N_MINORS;i++)
        {
                kfree(_tdev[i].cbuf.buf);

        }
        for(j=0;j<N_MINORS;j++)
        {

                cdev_del(&_tdev[j]._cdev);
	}
	        unregister_chrdev_region(deviceno,1);
}


module_init(driv_init);
module_exit(driv_exit);

